@extends('layouts.app')
@section('content')
<div>
<div class="w-full flex justify-center item-center">
<form action="/home/edit/{{$post->id}}" method="POST">
 @csrf   
<input type="text" name="name">    
<x-input class="border rounded flex" name="title" placeholder="title" value="{{$post->title}}" type='text'/>
<textarea name="description" class="border rounded flex" id="" cols="30" rows="10" value="{{$post->description}}"></textarea>
<button class="p-1 bg-green-100 text-green-500 rounded font-bold w-full py-3 pb-3" type="submit">Uuenda</button>
</form>    
</div>

</div>
@endsection