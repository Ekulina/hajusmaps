@extends('layout.app')
@section('content')
<div class="container mx-auto mt-8 mb-16 flex flex-wrap ">
    <div class=" flex justify-center w-full">
        <a class="rounded bg-red-200 px-6 py-3 mr-3 hover:bg-red-100 text-white" href="?name=joel">
        <span class="font-semibold">Joel</span>
        <a class="rounded bg-red-200 px-6 py-3 mr-3 hover:bg-red-100 text-white" href="?name=janek">
        <span class="font-semibold">Janek</span>
        <a class="rounded bg-red-200 px-6 py-3 mr-3 hover:bg-red-100 text-white" href="?name=">
        <span class="font-semibold">Me</span>
    </a>
    </div>  
    @foreach ($data as $item)
    @if ($name == 'joel')
    <div class="md:w-80 w-72 mx-6 mt-6 bg-white shadow-lg rounded">
        <img src="{{$item->image}}" alt="" class="object-contain h-56 w-full">
        <div class="px-2">
            <div class="mb-4 mt-2">
                <span class="font-semibold text-xl">{{$item->title}}</span>
                <p class="font-semibold text-gray-600 pr-4 text-lg">Color: {{$item->color}}</p>
            </div>
            <p class=" py-1 text-sm">{{$item->description}}</p>
            <p class="text-xl py-1 ">Coat:  <span class="text-gray-600 text-xl">{{$item->coat}}</span></p>
        </div>
    </div>
    @elseif($name == 'janek')
    <div class="md:w-80 w-72 mx-6 mt-6 bg-white shadow-lg rounded">
        <div class="px-2">
            <div class="mb-4 mt-2">
                <span class=" text-xl font-semibold">{{$item->title}}</span>
                <p class="font-semibold text-gray-600 pr-4 text-lg">Status: {{$item->status}}</p>
            </div>
            <p class=" py-1 text-sm">{{$item->body}}</p>
            <p class="text-sm py-1 ">Added date: <span class="text-gray-600 text-sm">{{$item->added}}</span></p>
        </div>
    </div>
    @else
    <div class="md:w-80 w-72 mx-6 mt-6 bg-white shadow-lg rounded">
        <img src="{{$item->image}}" alt="" class="object-contain h-56 w-full">
        <div class="px-2">
            <div class="mb-4 mt-2">
                <span class="font-semibold">{{$item->title}}</span>
                <span class="font-semibold pr-4 text-lg float-right">Size: {{$item->size}}</span>
            </div>
            <p class=" py-1 text-sm">{{$item->description}}</p>
            <p class="text-xl py-1 ">Colour:  <span class="text-red-400 text-xl">{{$item->colour}}</span></p>
        </div>
    </div>
    @endif
    @endforeach
</div>
@endsection