@extends('layout.app')
@section('content')
<div>
<div class="w-full flex justify-center item-center">
<form action="/edit/{{$marker->id}}" method="POST">
 @csrf   
<input  type="text" name="name">    
<x-input name="name" placeholder="asukoht" value="{{$marker->name}}" type='text'/>
<x-input name="lng" placeholder="laiuskraad" value="{{$marker->lng}}" type='text'/>
<x-input name="lat" placeholder="pikkuskraad" value="{{$marker->lat}}" type='text'/>
<x-input name="description" placeholder="kirjeldus" value="{{$marker->description}}" type='text'/>
<button class="p-1 bg-green-100 text-green-500 rounded font-bold w-full py-3 pb-3" type="submit">Uuenda</button>
</form>    
</div>

</div>
@endsection