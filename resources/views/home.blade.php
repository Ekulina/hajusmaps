@extends('layouts.app')

@section('content')
<div class="container">
    <div class="w-full flex justify-center item-center">
        <div></div>
        <form action="/home" method="POST">
         @csrf      
        <x-input name="title" placeholder="title" value="{{old('title')}}" type='text'/>
        <textarea name="description" class="border rounded flex" id="" cols="30" rows="10"></textarea>
        <b></b>
        <button class="p-1 bg-green-100 text-green-500 rounded font-bold w-full py-3 pb-3" type="submit">Salvesta</button>
        </form>    
        </div>
        <div class="bg-white shadow-md rounded my-6">
            <table class="min-w-max w-full table-auto">
                <thead>
                    <tr class="bg-green-200 text-green-600 uppercase text-sm leading-normal">
                        <th class="py-3 px-6 text-left">Title</th>
                        <th class="py-3 px-6 text-center">Description</th>
                        <th class="py-3 px-6 text-center">Actions</th>
                    </tr>
                </thead>
                <tbody class="text-gray-600 text-sm font-light">
                    @foreach ($posts as $post)
                        
                    
                    <tr class="border-b border-gray-200 hover:bg-gray-100">
                        <td class="py-3 px-6 text-left whitespace-nowrap">
                         {{$post->title}}   
                        </td>
                        
                        <td class="py-3 px-6 text-center">
                            {{$post->description}}
                        </td>
                        <td class="py-3 px-6 text-center">
                            <a class="p-1 bg-blue-100 text-blue-500 rounded" href="/home/edit/{{$post->id}}">Edit</a>
                            <a class="p-1 bg-red-100 text-red-500 rounded" href="/home/delete/{{$post->id}}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</div>
@endsection
