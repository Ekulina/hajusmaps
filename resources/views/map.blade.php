@extends('layout.app')
@section('content')
<v-map>
    <div>
    
        <div class="w-full flex justify-center item-center">
        <form action="/" method="POST">
         @csrf   
        <input type="text" name="name">    
        <x-input name="name" placeholder="asukoht" value="{{old('name')}}" type='text'/>
        <x-input name="lng" placeholder="laiuskraad" value="{{old('lng')}}" type='text'/>
        <x-input name="lat" placeholder="pikkuskraad" value="{{old('lat')}}" type='text'/>
        <x-input name="description" placeholder="kirjeldus" value="{{old('description')}}" type='text'/>
        <button class="p-1 bg-green-100 text-green-500 rounded font-bold w-full py-3 pb-3" type="submit">Salvesta</button>
        </form>    
        </div>
        <div class="bg-white shadow-md rounded my-6">
            <table class="min-w-max w-full table-auto">
                <thead>
                    <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                        <th class="py-3 px-6 text-left">Name</th>
                        <th class="py-3 px-6 text-left">Lng</th>
                        <th class="py-3 px-6 text-center">Lat</th>
                        <th class="py-3 px-6 text-center">Description</th>
                        <th class="py-3 px-6 text-center">Actions</th>
                    </tr>
                </thead>
                <tbody class="text-gray-600 text-sm font-light">
                    @foreach ($markers as $marker)
                        
                    
                    <tr class="border-b border-gray-200 hover:bg-gray-100">
                        <td class="py-3 px-6 text-left whitespace-nowrap">
                         {{$marker->name}}   
                        </td>
                        <td class="py-3 px-6 text-left">
                         {{$marker->lng}}   
                        </td>
                        <td class="py-3 px-6 text-center">
                            {{$marker->lat}}    
                        </td>
                        <td class="py-3 px-6 text-center">
                            {{$marker->description}}
                        </td>
                        <td class="py-3 px-6 text-center">
                            <a class="p-1 bg-blue-100 text-blue-500 rounded" href="/edit/{{$marker->id}}">Edit</a>
                            <a class="p-1 bg-red-100 text-red-500 rounded" href="/delete/{{$marker->id}}">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </v-map>
@endsection