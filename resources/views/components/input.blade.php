@props(['name','placeholder','value', 'type'])
<div class="pb-4">
<input class="focus:outline-none w-full border rounded" value="{{$value}}" type="{{$type}}" name="{{$name}}" placeholder="{{$placeholder}}" id="{{$name}}">
@error($name)
    <div class="text-red-500 text-xs mt-1">{{$message}}</div>
@enderror
</div>