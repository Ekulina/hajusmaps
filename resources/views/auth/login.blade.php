@extends('layouts.app')

@section('content')
<div class="container">
    <div class="w-full flex justify-center item-center">

        <div class="border w-72 rounded shadow-sm">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="p-6">
                    <p class="font-semibold text-lg pb-4">
                        Login
                    </p>
                    <x-input name="email" placeholder="email" value="{{old('email')}}" type='email'/>
                    <x-input name="password" placeholder="parool" value="{{old('password')}}" type='password'/>
                
                    <button type="submit" class="w-full p-2 rounded text-center bg-green-50">
                        {{ __('Login') }}
                    </button>
                </div>
                
            </form>
        </div>
</div>
</div>
@endsection
