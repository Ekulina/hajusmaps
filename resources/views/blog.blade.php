@extends('layout.app')
@section('content')
<div class='container mx-auto'>
<h1 class="font-semibold text-3xl tracking-tight p-3">
    Blog
</h1>
<form  method="get" action="/home"><button class= "px-3 bg-green-100 text-green-500 rounded font-bold text-l mb-3"type="submit">Login
</button></form>
<div>
    @foreach ($posts as $post)
    <div class='border rounded shadow-sm w-9/12 p-3 bg-green-50 px-8 mb-6'>
        <div class='p-4'> 
        <h1 class="font-semibold p-3">{{$post->title}}</h1>
         <p class="p-3">
            {{$post->description}}   
        </p> 
    </div>
    <div class="px-4">
    <form action="/comments/add" method="POST">
    @csrf
    <input type="hidden" name="blog_id" value="{{$post->id}}">
    <textarea class="border rounded w-full" name="comments" id="" cols="30" rows="4"></textarea>
    <button type="submit" class="p-1 bg-blue-100 text-blue-500 rounded">Add comment</button>
    </form>    
    </div>
    <div>
        @foreach ($post->comments() as $comment)
        <p class="p-3">
        {{$comment->comments}}
        </p>    
        @if (Auth::check())
            <a href="/comments/delete/{{$comment->id}}" class="p-1 bg-red-100 text-red-500 rounded">Delete comment</a>
        @endif    
        @endforeach
    </div> 
    </div> 
    @endforeach
</div>

</div>
@endsection
