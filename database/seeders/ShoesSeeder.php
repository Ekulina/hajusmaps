<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ShoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      foreach ($this->shoes as $shoe){
        \App\Models\Shoes::create($shoe);

      }  
    }     
    protected array $shoes = [
       [
       "title"=> "Balerinas",
       "description"=> "Low comfy shoes.",
       "image"=> "https://cdn.pixabay.com/photo/2015/11/08/14/14/boot-tick-1033641_960_720.jpg",
       "size"=> "38",
       "colour"=> "green",
    ],
    [
        "title"=> "Shoes",
        "description"=> "Comfortable sporty shoes.",
        "image"=> "https://cdn.pixabay.com/photo/2013/09/12/19/57/boots-181744_960_720.jpg",
        "size"=> "40",
        "colour"=> "leopard pattern",
     ],

     [
        "title"=> "High heels",
        "description"=> "Beautiful, high heeled shoes.",
        "image"=> "https://cdn.pixabay.com/photo/2013/07/12/19/21/pumps-154636_1280.png",
        "size"=> "36",
        "colour"=> "red",
     ],

     [
        "title"=> "Sneakers",
        "description"=> "Sneakers with an attitude.",
        "image"=> "https://cdn.pixabay.com/photo/2014/09/03/20/15/shoes-434918_960_720.jpg",
        "size"=> "40",
        "colour"=> "black",
     ],

     [
      "title"=> "Sandals",
      "description"=> "Free you toes.",
      "image"=> "https://media.istockphoto.com/photos/the-man-feet-wearing-sport-sandals-selective-focus-picture-id1208006730?s=612x612",
      "size"=> "40",
      "colour"=> "black",
   ],
       ];
}
