<?php
namespace Deployer;
require 'recipe/laravel.php';
require 'contrib/rsync.php';
// Project name
set('application', 'hajusmaps');
set('remote_user', 'virt82371');
set('http_user', 'virt82371');
set('keep_releases', 2);
// Project repository
set('repository', 'git@gitlab.com:Ekulina/hajusmaps.git');
// [Optional] Allocate tty for git clone. Default value is false.
// Hosts
host('ta18pupart.itmajakas.ee')
    ->setHostname('ta18pupart.itmajakas.ee')
    ->set('http_user', 'virt82371')
    ->set('deploy_path', '/data01/virt82371/domeenid/www.ta18pupart.itmajakas.ee/hajusmaps');
// Tasks
task('deploy:update_code', function () {
    upload(__DIR__ . "/", '{{release_path}}', [
        'options' => [
            '--exclude deploy.php',
            '--exclude node_modules/',
            '--exclude .env',
            '--exclude .git/',
            '--exclude storage/',
        ]
    ]);
});
//Restart opcache
task('opcache:clear', function () {
    run('killall php74-cgi || true');
})->desc('Clear opcache');
task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:migrate',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'deploy:publish'
]);
after('deploy:failed', 'deploy:unlock');