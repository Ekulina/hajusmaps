<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarkerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\ShoesController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MarkerController::class, "index"]);

Route::post('/', [MarkerController::class, "store"]);
Route::get('/delete/{id}', [MarkerController::class, "destroy"]);
Route::get('/edit/{id}', [MarkerController::class, "edit"]);
Route::post('/edit/{id}', [MarkerController::class, "update"]);
Auth::routes();
Route::middleware('auth')->group(function () {
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/home', [HomeController::class, "store"]);
Route::get('/home/delete/{id}', [HomeController::class, "destroy"]);
Route::get('/home/edit/{id}', [HomeController::class, "edit"]);
Route::post('/home/edit/{id}', [HomeController::class, "update"]);
});

Route::get('/blog', [App\Http\Controllers\HomeController::class, 'blog']);

Route::post('/comments/add', [CommentsController::class, "store"]);
Route::get('/comments/delete/{id}', [CommentsController::class, "destroy"]);

Route::get('/shoes', [ShoesController::class, "others"]);
