<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarkerController;
use App\Http\Controllers\ShoesController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("/shoes", [ShoesController::class, "shoes"]);
Route::get("/markers", [MarkerController::class, "api"]);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
