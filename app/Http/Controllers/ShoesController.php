<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use App\Models\Shoes;
use Illuminate\Http\Request;

class ShoesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Shoes::all();
        $seconds = 60;
        $time = date("Y-m-d H:i:s");
        Cache()->put('shoes', $response, $seconds);
        Cache()->put('time', $time, $seconds);
    }

    public function shoes()
    {
        if (Cache()->has('shoes') && Cache()->has('time')) {
            $data = Cache()->get('shoes');
            $time = Cache()->get('time');
            return [
                'time' => $time,
                'data' => $data
            ];
        } else {
            $this->index();
            return redirect('api/shoes');
        }

    }

    public function others(Request $request)
    {
        $name = $request->name;
        switch ($name) {
            case 'joel':
                $link = "https://ta19pupart.itmajakas.ee/hajus5/api";
                break;
            case 'janek':
                $link = "https://ta19.janek.itmajakas.ee/code/hajusrakendused/favorite-api/api/?param=10";
                break;
            default:
                $link = "https://hajusmaps.ta18pupart.itmajakas.ee/api/shoes";
        }
        $apiData = Http::get($link);
        $data = json_decode($apiData);
        return view('shoes', [
            'data' => $data->data,
            'name' => $name
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shoes  $shoes
     * @return \Illuminate\Http\Response
     */
    public function show(Shoes $shoes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shoes  $shoes
     * @return \Illuminate\Http\Response
     */
    public function edit(Shoes $shoes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shoes  $shoes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shoes $shoes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shoes  $shoes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shoes $shoes)
    {
        //
    }
}
