<?php

namespace App\Http\Controllers;

use App\Models\marker;
use Illuminate\Http\Request;

class MarkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marker = marker::all();
        return view("map", [
            "markers" => $marker
        ]);
    }
    public function api()
    {
        return marker::all();

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        marker::create($request->validate([
            "name" => "required",
            "lng" => "required|numeric",
            "lat" => "required|numeric",
            "description" => "required",
        ]));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\marker  $marker
     * @return \Illuminate\Http\Response
     */
    public function show(marker $marker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\marker  $marker
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marker = marker::find($id); 
        return view("edit", [
            "marker" => $marker
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\marker  $marker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $marker = marker::find($id);
        $marker->update($request->validate([
            "name" => "required",
            "lng" => "required|numeric",
            "lat" => "required|numeric",
            "description" => "required",
        ]));
        return redirect("/");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\marker  $marker
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $marker = marker::find($id);
        $marker->delete();
        return redirect()->back(); 
    }
}
