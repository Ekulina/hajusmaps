<?php

namespace App\Http\Controllers;
use App\Models\blog;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = blog::all();
        return view('home', [
            'posts' => $posts
        ]);
    }
    public function store(Request $request)
    {
        blog::create($request->validate([
            "title" => "required",
            "description" => "required",
        ]));
        return redirect()->back();
    }
    public function edit($id)
    {
        $post = blog::find($id); 
        return view("editp", [
            "post" => $post
        ]); 
    }
    public function update(Request $request, $id)
    {
        $post = blog::find($id);
        $post->update($request->validate([
            "title" => "required",
            "description" => "required",
        ]));
        return redirect("/home");

    }
    public function destroy($id)
    {
        $post = blog::find($id);
        $post->delete();
        return redirect()->back(); 
    }
    public function blog()
    {
        $posts = blog::all();
        return view('blog', [
            'posts' => $posts
        ]);
    }
}
