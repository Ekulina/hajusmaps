<?php

namespace App\Http\Controllers;
use App\Models\comments;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Request $request)
    {
        comments::create($request->validate([
            "comments" => "required",
            "blog_id" => "required",
        ]));
        return redirect()->back(); 

    }
    public function destroy($id)
    {
        $comment = comments::find($id);
        $comment->delete();
        return redirect()->back(); 
    }
}
