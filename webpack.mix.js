const mix = require('laravel-mix');



mix.js('resources/js/app.js', 'public/js').vue()

    .postCss('resources/css/app.css', 'public/css', [

        require("tailwindcss"),

    ])

    .webpackConfig({

        devServer: {

            host: '0.0.0.0',

            port: 8080,

        }

    });